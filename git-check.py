#!/usr/bin/env python3

import os
import re
from git import Repo # pip install GitPython

behind_or_ahead_regex = re.compile("\[(behind|ahead) \d+\]$")
behind_or_ahead_regex = re.compile("\[ahead \d+\]$")

def get_default_branch(repo):
    for branch in repo.references:
        if branch.name == "refs/heads/master":
            return "master"
        elif branch.name == "refs/heads/main":
            return "main"
    return None

def get_repo_changes(repo):
    changes = []
    default_branch = get_default_branch(repo)

    repo.remotes.origin.fetch(prune=True)

    if repo.index.diff(repo.head.commit):
        changes.append(f"Changes")

    if repo.index.diff(None):
    # if repo.head.commit != repo.head.commit.tree:
        changes.append(f"Uncommitted changes")

    if default_branch and repo.head.commit != repo.remotes.origin.refs[default_branch].commit:
        changes.append(f"Remote changes")

    if repo.is_dirty():
        changes.append(f"Untracked files")

    if repo.index.diff(None):
        changes.append(f"Staged but uncommitted changes")

    if repo.is_dirty(untracked_files=True):
        changes.append(f"Changes in stash")

    local_branches = [ref.name.split('/')[-1] for ref in repo.references if ref.name.startswith('refs/heads/')]

    if local_branches:
        changes.append(f"Local branches found in repository '{repo_name}': {', '.join(local_branches)}")

    gone_branches  = [ref.name.split('/')[-1] for ref in repo.remotes.origin.refs if "[gone]" in ref.name]

    # gone_branches = set(local_branches) - set(remote_branches)

    if gone_branches:
        changes.append(f"Gone branches found in repository '{repo_name}': {', '.join(gone_branches)}")

    references = repo.git.for_each_ref(format="%(refname:short) %(upstream:track)", sort="refname").split("\n")
    for ref in references:
    #     if ref.endswith(" [gone]"):    
    #         changes.append(f"{ref}")
        if behind_or_ahead_regex.search(ref):
            changes.append(f"{ref}")

    stash = repo.git.stash("list")
    if stash:
        changes.append(f"Stashed changes: {stash}")

    formated_branches = repo.git.branch(format="%(refname:short) -^%(upstream)").split("\n")
    for formated_branch in formated_branches:
        if formated_branch.endswith("-^"):
            branch = formated_branch[:-3]
            changes.append(f"Local branch {branch}")

    return changes
    

def check_for_changes_in_repositories(root_dir):
    for root, dirs, _ in os.walk(root_dir):
        if ".git" in dirs:
            repo_path = os.path.join(root, ".git")
            repo = Repo(repo_path[:-4])  # Remove "/.git" from the end
            repo_name = os.path.basename(repo.working_dir)

            changes = get_repo_changes(repo)
            if changes:
                print(f"{repo_name}")
                for change in changes:
                    print(f"\t{change}")

            # upstream_tracking_refs = [
            #     ref.split(":", 1)[0] for ref in references if ref.endswith(":track")
            # ]
            # 
            # if upstream_tracking_refs:
            #     changes.append(f"References with upstream tracking in repository '{repo_name}':")
            #     for ref in upstream_tracking_refs:
            #         changes.append(f"- {ref}")


if __name__ == "__main__":
    target_directory = "/home/pedro.david/Documents/Projects/commons"
    check_for_changes_in_repositories(target_directory)
